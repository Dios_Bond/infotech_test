const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const path = require('path');
html = require('html');

const app = express();

const config = require('./config/config');
let users = require('./test_data/users');

//Register cookie parser and body parser
app.use(cookieParser());
app.use(bodyParser.json());

app.use(session({
	//  store: store, need samesing store
	resave: false,
	saveUninitialized: true,
	secret: 'secret key'
}));

app.set('view engine', 'ejs');

app.get('/', function (req, res) {
	res.sendFile(path.join(__dirname, './public/login.html'));
});


app.post('/login', function (req, res) {

    let f_user;
    //console.log(await req.params.name);
	// поиск пользователя в массиве users 
	for (let i = 0; i < users.length; i++) {
		let user = users[i];
		if (user.name == req.body.username && user.pass == req.body.password) {
            f_user = {
                name : user.name,
                type : user.type
            }

            
			break;
		}
    }
    //console.log(f_user);
	if (f_user !== undefined) {
        req.session.username = f_user.name;
        req.session.usertype = f_user.type;
		console.log("Login is OK: ", req.session.username);
        //res.send('Login is OK' + '\nsession ID: ' + req.session.id + '\nuser: ' + req.session.username + '\ntype: ' + req.session.usertype);

        if (f_user.type == "admin") {
            //res.redirect(301,'./admin')
            res.send(f_user.type);
            res.end();
            //res.sendFile(path.join(__dirname, './public/admin.html'));
        }
        else {
            res.redirect(301, 'user')
        }
	} else {
		console.log("Login error by user: ", req.body.username)
		res.status(401).send('Login or password error');
	}
});

//Route for logout session
app.get('/logout', function (req, res) {
    console.log('logout route');
    req.session.username = '';
    res.send('logout!');
});

//Route to admin page with all ToDo
app.get('/admin', async function (req, res) {
    console.log('request admin page');
    console.log(req.session.username);
    checkUser(req).then(prom => {
        console.log('RES', prom);
    
        if (prom == "admin") {
            //res.sendFile(path.join(__dirname, './public/admin.html'), prom);
            res.render(path.join(__dirname, './public/admin'), { name: 'bla-bla-bla' })
        }
        else {
            res.send("Acess to page Denied")
        }
    })
});

//Route to user page with personal ToDo

app.get('/user', function (req, res) {
    console.log('request user page');
    checkUser(req).then(prom => {
        console.log('RES', prom);
        if (prom == "user") {
            //res.sendFile(path.join(__dirname, './public/admin.html'));
            res.send('<h1>User page</h1>')
        }
        else {
            res.send("Acess to page Denied")
        }
    })
});


console.log(users);

async function checkUser(req) {
    console.log("in function", req.session.username)
    let type
    for (let i = 0; i < users.length; i++) {
		let user = users[i];
		if (user.name == req.session.username) {
            //console.log(await username);
                type = user.type
        }
    }
    console.log('found user type', type);
    return type
}


app.listen(config.server.port, function () {
	console.log(config.nameapp, 'run on port ', config.server.port);
});